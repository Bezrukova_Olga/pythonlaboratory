def is_digit(string):
    if string.isdigit():
        return True
    else:
        try:
            float(string)
            return True
        except ValueError:
            return False


def is_operation(string):
    if string == '+' or string == '-' or string == '*' or string == '=':
        return True


def result(a1, b1, c1):
    if a1 == 0:
        x = -c1 / b1
        print('x = ' + str(x))
    else:
        discriminant = b1 ** 2 - 4 * a1 * c1
        print('Дискриминант = ' + str(discriminant))
        if discriminant == 0:
            x = -b1 / (2 * a1)
            print('x = ' + str(x))
        else:
            x1 = (-b1 + discriminant ** 0.5) / (2 * a1)
            x2 = (-b1 - discriminant ** 0.5) / (2 * a1)
            print('x1 = ' + str(x1))
            print('x2 = ' + str(x2))


def empty_line(string):
    count_emp = 0
    count_sym = 0
    for j in range(len(string)):
        if string[j] == ' ':
            count_emp += 1
        else:
            count_sym += 1
    if count_emp == count_sym:
        return True
    else:
        return False


def find_a(string):
    coef_a = ''
    for j in range(len(string) - 2):
        if string[j] == 'x' and string[j + 2] == 'x':
            k = j - 1
            while (string[k] != '+' and string[k] != '-') and k > 0:
                k -= 1
                coef_a += string[k]
            return coef_a[::-1]


def find_b(string):
    coef_b = ''
    for j in range(len(string) - 2):
        if string[j] == 'x' and string[j + 2] != 'x' and string[j - 2] != 'x':
            k = j - 1
            while string[k] != '+' and string[k] != '-' and k > 0:
                k -= 1
                coef_b += string[k]
            return coef_b[::-1]


def find_c(string):
    coef_c = ''
    for j in range(len(string) - 1):
        if is_digit(string[j]) and string[j - 1] != 'x' and string[j + 1] != 'x' and string[j + 2] != 'x':
            k = j
            while string[k] != '+' and string[k] != '-' and k > 0:
                coef_c += string[k]
                k -= 1
            coef_c += string[k]
            return coef_c[::-1]


equation = input('Введите уравнение вида a*x*x+b*x+c=0: ')
if len(equation) != 0 and not empty_line(equation):
    coefficient = []
    operation = []
    for i in range(len(equation)):
        if is_digit(equation[i]):
            coefficient.append(equation[i])
        if is_operation(equation[i]):
            operation.append(equation[i])
    a = float(find_a(equation))
    b = float(find_b(equation))
    c = float(find_c(equation))
    print(a, ' ', b, ' ', c)
    result(a, b, c)
else:
    print('Введенная строка - пустая')
