import random


def create_array():
    if is_digit(n) and is_digit(m):
        for i in range(int(n)):
            array.append([])
            for j in range(int(m)):
                array[i].append(random.randint(0, 20))
        print("Исходная:")
        for u in range(int(n)):
            print(array[u])
        print()
    else:
        print("Error! You enter invalid data")


def is_digit(string):
    if string.isdigit():
        return True
    else:
        return False


def is_value(string):
    if string.isdigit():
        return True
    else:
        try:
            float(string)
            return True
        except ValueError:
            return False


def unique_number(array_el):
    unique_set = []
    for i in range(len(array_el)):
        unique_set = set(list(unique_set) + array_el[i])
    unique_set = list(unique_set)
    for i in range(len(array_el)):
        for j in range(len(array_el[i])):
            count = count_elements(array_el[i][j], array_el)
            for k in range(len(list(unique_set))):
                if array_el[i][j] == list(unique_set)[k] and count > 1:
                    del unique_set[k]
                    break

    print('Числа, которые встречаются один раз: ', unique_set)


def count_elements(element, array_c):
    count = 0
    for i in range(len(array_c)):
        count += array_c[i].count(element)
    return count


def from_file():
    data = []
    with open("text.txt") as f:
        for line in f:
            data.append([x for x in line.split()])
    for i in range(len(data) - 1):
        if len(data[i]) != len(data[i+1]):
            print('Матрица имеет пустые ячейки')
            data = []
            return data
    for i in range(len(data)):
        for j in range(len(data[i])):
            if not is_value(data[i][j]):
                print('Некорректные данные на позиции (', i+1, ',', j+1, ')')
                data = []
                return data
    return data


data_from_file = from_file()
n = input("Введите количество строк: ")
m = input("Введите количество столбцов: ")
unique_number_array = []
array = []
create_array()
unique_number(array)
if not len(data_from_file) == 0:
    unique_number(data_from_file)
